import uuid
from datetime import datetime

from flask_login import UserMixin

from . import db, login_manager


def generate_uuid():
    return str(uuid.uuid4())


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


class User(UserMixin, db.Model):
    id = db.Column(db.String(300), primary_key=True, default=generate_uuid)
    first_name = db.Column(db.String(200), nullable=False)
    last_name = db.Column(db.String(200))
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    created_data = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "<User %r>" % self.id


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    permission = db.Column(db.JSON)
