from flask import Blueprint, session, request, redirect, url_for

from project import db
from project.models import User

main = Blueprint('main', __name__)

from flask import render_template
from flask_login import login_required, current_user


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/profile')
@login_required
def profile():
    user_id = session['user_id']
    user = User.query.filter_by(id=user_id).first()

    return render_template('profile.html', name=current_user.first_name, user=user)


@main.route('/update_profile', methods=['POST'])
def update_profile():
    user_id = session['user_id']
    user = User.query.filter_by(id=user_id).first()

    # Обновить информацию о пользователе на основе данных из формы
    user.first_name = request.form['first_name']
    user.last_name = request.form['last_name']
    user.email = request.form['email']

    # Сохранить изменения в базе данных
    db.session.commit()

    return redirect(url_for('main.profile'))
